import PropTypes from 'prop-types';

const InputPropType = {
    id: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    color: PropTypes.string,
    classes: PropTypes.instanceOf(Object).isRequired,
    onChange: PropTypes.func,
};

export default InputPropType;
