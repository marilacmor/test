import PropTypes from 'prop-types';

const StandarCardPropType = {
    item: PropTypes.object.isRequired,
    classes: PropTypes.instanceOf(Object).isRequired,
};

export default StandarCardPropType;
