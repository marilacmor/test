import PropTypes from 'prop-types';

const DashboardPropType = {
  name: PropTypes.string,
  classes: PropTypes.instanceOf(Object).isRequired,
  onChange: PropTypes.func,
};

export default DashboardPropType;
