import PropTypes from 'prop-types';

const StandarButtonPropType = {
    title: PropTypes.string,
    color: PropTypes.string,
    handleSubmit: PropTypes.func,
};

export default StandarButtonPropType;
