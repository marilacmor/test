import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import StandarCardPropType from '../../PropTypes/StandarCardPropType'; 
import { imagen } from '../../Public/Imagen/imagen';

const StandarCard = ({ item,  classes }) => (
    <Card className={classes.card}>
        <CardActionArea>
            <CardContent className={classes.root3}>
                <Typography variant="body2" color="textPrimary" component="h1">
                <b>Nombre:</b> {item.name}
                </Typography>
            </CardContent>
            <CardMedia
                component="div"
                title="user"
                className={classes.media}
            >
                {item.imagen === "m" ? (
                    <img src={imagen.woman.imagen} alt="..." className={classes.profilePic} ></img>
                ) : (
                    <img src={imagen.man.imagen} alt="..." className={classes.profilePic}></img>
                )}
            </CardMedia>
        </CardActionArea>
    </Card>
);

StandarCard.propTypes = StandarCardPropType;

export default StandarCard;
  
