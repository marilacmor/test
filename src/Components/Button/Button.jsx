import React from 'react';
import Button from '@material-ui/core/Button';
import StandarButtonPropType from '../../PropTypes/StandarButtonPropType'; 

const StandarButton = ({ title, color, onClick }) => (
    <Button
        variant="contained"
        color={color}
        onClick={onClick}
        >
        {title}
    </Button>
);

StandarButton.propTypes = StandarButtonPropType;

export default StandarButton;
  
