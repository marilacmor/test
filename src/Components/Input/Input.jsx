import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputPropType from '../../PropTypes/InputPropType'; 

const Input = ({ id, name, label, color, value, onChange, classes }) => (
    <TextField
        id={id}
        label={label}
        variant="outlined"
        color={color}
        name={name}
        value={value}
        onChange={onChange}
        className={classes.textFieldWidth}
    />
);

Input.propTypes = InputPropType;

export default Input;
  
