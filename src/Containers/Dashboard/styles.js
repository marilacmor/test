export const customStyles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
      },
      gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
      },
      title: {
        color: theme.palette.primary.light,
      },
      root2: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
      },
      card: {
        maxWidth: 345,
      },
      media: {
        height: 200,
      },
      root3: {
        width: 314,
        maxWidth: 314,
      },
      textFieldWidth: {
        width: 300,
        maxWidth: 300,
      },
      divForm: {
        marginBottom: 60,
        width: 574,
        padding: 40,
        margin: 'auto',
      },
      profilePic: {
        width: 200,
        height: 200,
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
      }
  });