import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Input from '../../Components/Input/Input';
import StandarButton from '../../Components/Button/Button';
import StandarCard from '../../Components/Card/Card';
import { addUser } from '../../Actions/UserAction';
import DashboardPropType from '../../PropTypes/DashboardPropType';
import { customStyles } from './styles';


export class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
  }

  clearForm = () => {
    const name = "";
    this.setState({ name });
  }

  handleChange = e => {
    e.preventDefault();
    let attr = e.target.name;
    let value = e.target.value;
    this.setState({ [attr]: value });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const { name } = this.state;
    await this.props.addUser(name);
    this.clearForm();
  };

  render() {
    const { users, classes} = this.props;
    return (
      <div>
        <div className={classes.divForm}>
          <form noValidate autoComplete="off">
            <Grid container spacing={1}>
              <Grid container item xs={6} spacing={0}>
                <Input id="name" name="name" label="Nombre" color="primary" value={this.state.name} onChange={this.handleChange} classes={classes} />
              </Grid>
              <Grid container item xs={6} spacing={0}>
                <StandarButton title="Agregar" color="primary" onClick={this.handleSubmit} />
              </Grid>
            </Grid>
          </form>
        </div>
        <div className={classes.root}>
          <div className={classes.root}>
            <Grid container spacing={3}>
              {users.map(item => (
                  <Grid item xs={4} key={item.name}>
                    <StandarCard item={item} classes={classes} />
                  </Grid>
              ))}
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.userReducer.users,
});

const mapDispatchToProps = {
  addUser
};

Dashboard.propTypes = DashboardPropType;

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  withStyles(customStyles)(Dashboard),
);