import React, { Component } from 'react';

import './MainLayout.css';

class MainLayout extends Component {
  render() {
    const { children } = this.props;
    return (
      <div className="main-layout">
        <div>{children}</div>
      </div>
    );
  }
}

export default MainLayout;
