
export const ACTIONS = {
  ADD_USER: 'user/add_user',
};

function randomImagen() {
  var characters = 'mf';
  return characters.charAt(Math.floor(Math.random() * 2));
}

export const addUser = (name) => dispatch => {
  let imagen = randomImagen();
  dispatch({ type: ACTIONS.ADD_USER, payload: { name, imagen } });
};
