import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import MainLayout from '../Containers/MainLayout/MainLayout';
import Dashboard from '../Containers/Dashboard/Dashboard';

const PublicRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={props =>
          <MainLayout>
            <Component {...props} />
          </MainLayout>
      }
    />
  );
};

class Routes extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <PublicRoute exact path="/" component={Dashboard} />
        </Switch>
      </Router>
    );
  }
}


export default Routes;
