import { ACTIONS } from '../Actions/UserAction';

const initialState = {
  users: [
    {
      "name": "Jorge Gonzalez",
      "imagen": "m"
    },
    {
      "name": "Juana Perez",
      "imagen": "f"
    }
  ],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.ADD_USER:
      return {
        ...state,
        users: [...state.users, action.payload],
      }
    default:
      return state;
  }
};

export default userReducer;
